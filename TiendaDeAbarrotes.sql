create database abarrotespueblito;
use abarrotespueblito;
#region CreacionTablas
create table proveedor(idProveedor int primary key not null auto_increment,nombreP varchar(50), DireccionP varchar(100),
telefono varchar(20));
create table categorias(idCategoria int primary key not null auto_increment, nombreC varchar(50));
create table productos(idProductos int primary key not null auto_increment, nombreProd varchar(50), Precio double,fkCategoria int,
foreign key (fkCategoria) references categorias(idCategoria));
create table surtido(idSurtido int primary key not null auto_increment, fkProveedor int, fkProducto int, Cantidad int,
fecha date, foreign key (fkProveedor) references proveedor(idProveedor), foreign key (fkProducto) references productos (idProductos));
#end Region
#Region insertar
insert into categorias values(null,'Refresco'),(null,'Frituras'),(null,'Galletas'),(null,'Pan');
insert  into proveedor values(null,'Jos� P�rez','Enrique Segoviano #22','4743654811'),(null,'Gustavo Mendoza','El Paco #67','3951185562');
insert into productos values(null,'CocaCola',12.5,1),(null,'Sabritas',13,2),(null,'Principe',10,3),(null,'Puerconcha',6,4);
insert into surtido values(null,1,4,30,'2020-02-5'),(null,2,1,100,'2020-01-01');

insert into productos values(null,'Fanta',10,1);
#end region
#region  Consulta
select nombreC, nombreProd from categorias,productos where productos.fkCategoria = categorias.idCategoria order by nombreC desc;
select * from productos;
#end region
#Region Procedimiento
create procedure insertarVenta(in _fkPv int, in _fkPrd int, in _cant int, in _fch date)
begin
if _cant >=1  then 
insert into surtido values(null,_fkPv,_fkPrd,_cant,_fch);
else 
select "Error. Dato Nulo o negativo";
end if;
end;

call insertarVenta(1,1,null,'2020-01-15');
select * from surtido;
#end region
#Region Vista
create view Surtir as select nombreP as 'Proveedor', nombreProd as 'Producto', nombreC as 'Categoria', Cantidad as 'Cantidad',
Precio from proveedor,productos, categorias, surtido where idProveedor = fkProveedor and idCategoria = fkCategoria and idProductos = fkProducto;
select * from surtir;
#end region